package com.mevg.eva2_7_xml;

import android.os.AsyncTask;
import android.os.Environment;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by yjm_e on 14/11/2016.
 */

public class DoInBackground extends AsyncTask<String, Void, String> {
    TextView tv;

    public DoInBackground(TextView tv) {
        this.tv = tv;
    }

    @Override
    protected String doInBackground(String... strings) {
        String xmlFileName = strings[0];
        return parser(xmlFileName);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        tv.setText(s.toString());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

    }

    private String parser(String xml){
        StringBuilder builder = new StringBuilder();
        try{
            String file = Environment.getExternalStorageDirectory().getPath() + "/" + xml;
            InputStream is = new FileInputStream(file);
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = docBuilder.parse(is);

            if(document == null){
                return "ERROR";
            }
            NodeList listCommonTag = document.getElementsByTagName("COMMON");
            NodeList listBotanicalTag = document.getElementsByTagName("BOTANICAL");
            NodeList listZoneTag = document.getElementsByTagName("ZONE");
            NodeList listLightTag = document.getElementsByTagName("LIGHT");
            NodeList listPriceTag = document.getElementsByTagName("PRICE");
            NodeList listAvailabilityTag = document.getElementsByTagName("AVAILABILITY");

            builder.append(getAllData(listCommonTag));
            builder.append(getAllData(listBotanicalTag));
            builder.append(getAllData(listZoneTag));
            builder.append(getAllData(listLightTag));
            builder.append(getAllData(listPriceTag));
            builder.append(getAllData(listAvailabilityTag));

        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(ParserConfigurationException e){
            e.printStackTrace();
        }catch (SAXException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    private Object getAllData(NodeList list){
        StringBuilder builder = new StringBuilder();

        for(int i= 0; i < list.getLength(); i++){
            Node node = list.item(i);
            int size = node.getAttributes().getLength();
            String text = node.getTextContent();
            builder.append(i + " - " +text +"\n");

            for(int j = 0; j < size; j++){
                String attrName = node.getAttributes().item(j).getNodeName();
                String attrvalue = node.getAttributes().item(j).getNodeValue();
                builder.append(j + " - " + attrName + " - " +attrvalue +"\n");
            }
        }
        return builder;
    }
}
