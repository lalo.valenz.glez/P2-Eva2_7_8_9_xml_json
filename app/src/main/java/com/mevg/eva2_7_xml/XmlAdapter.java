package com.mevg.eva2_7_xml;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by yjm_e on 13/11/2016.
 */

public class XmlAdapter extends ArrayAdapter<Cd> {

    LayoutInflater inflater;

    public XmlAdapter(Context context, int resource, List<Cd> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View  view = convertView;

        if(view == null){
            inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.xml_view,null);
        }

        Cd  cd = getItem(position);

        if(cd != null){
            TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            TextView tvArtist = (TextView) view.findViewById(R.id.tvArtist);
            TextView tvCountry = (TextView) view.findViewById(R.id.tvCountry);
            TextView tvCompany = (TextView) view.findViewById(R.id.tvCompany);
            TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
            TextView tvYear = (TextView) view.findViewById(R.id.tvYear);

            if(tvTitle != null){
                tvTitle.setText(cd.getTitle());
            }
            if(tvArtist != null){
                tvArtist.setText("Artist: "+cd.getArtist());
            }
            if(tvCountry != null){
                tvCountry.setText("Country: "+cd.getCountry());
            }
            if(tvCompany != null){
                tvCompany.setText("Company: "+cd.getCompany());
            }
            if(tvPrice != null){
                tvPrice.setText("Price: $"+cd.getPrice());
            }
            if(tvYear != null){
                tvYear.setText("Year: "+cd.getYear());
            }

        }

        return view;
    }
}
