package com.mevg.eva2_7_xml;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {
    Button btnXml;
    Button btnXmld;
    Button btnJson;
    ListView lvDatos;
    TextView tvShow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnXml = (Button) findViewById(R.id.btnxml);
        btnXmld = (Button) findViewById(R.id.btnxml2);
        btnJson = (Button) findViewById(R.id.btnjson);
        lvDatos = (ListView) findViewById(R.id.lvMostrar);
        tvShow = (TextView) findViewById(R.id.show);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1000);
        }
        else{
            btnXmld.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String xml = "plant_catalog.xml";

                    new DoInBackground(tvShow).execute(xml);
                }
            });
        }

        btnXml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                XmlAdapter adapter = new XmlAdapter(MainActivity.this,R.layout.xml_view, readXml());
                lvDatos.setAdapter(adapter);

            }

        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1000){
            Toast.makeText(MainActivity.this, permissions[0] + " " + grantResults[0], Toast.LENGTH_LONG).show();
            Toast.makeText(MainActivity.this, permissions[1] + " " + grantResults[1], Toast.LENGTH_LONG).show();
            btnXmld.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String xml = "plant_catalog.xml";

                    new DoInBackground(tvShow).execute(xml);
                }
            });
        }
    }

    public List<Cd> readXml(){
        List<Cd> list = new ArrayList<>();
        Cd cd = null;
        String text = "" ;
        try{
            XmlPullParser parser = getResources().getXml(R.xml.music);
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("cd")) {
                            // create a new instance of employee
                            cd = new Cd();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("cd")) {
                            // add employee object to list
                            list.add(cd);
                        } else if (tagname.equalsIgnoreCase("title")) {
                            cd.setTitle(text);
                        } else if (tagname.equalsIgnoreCase("artist")) {
                            cd.setArtist(text);
                        } else if (tagname.equalsIgnoreCase("country")) {
                            cd.setCountry(text);
                        } else if (tagname.equalsIgnoreCase("company")) {
                            cd.setCompany(text);
                        } else if (tagname.equalsIgnoreCase("price")) {
                            cd.setPrice(Double.parseDouble(text));
                        }else if (tagname.equalsIgnoreCase("year")) {
                            cd.setYear(Integer.parseInt(text));
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list.isEmpty() ? null : list;
    }
}
